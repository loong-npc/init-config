#!/bin/bash

# 配置文件和目录
SSH_CONFIG_DIR="/etc/ssh/sshd_config.d"
NEW_CONFIG_FILE="$SSH_CONFIG_DIR/99-custom-ssh-port-and-keys.conf"
SSH_SOCKET_CONF="/etc/systemd/system/ssh.socket"  # 自定义 socket 配置路径

# 公钥 URL（根据需要替换）
PUB_KEY_URL="https://gitlab.com/loong-npc/init-config/-/raw/main/public_key.pub"

# ---------------------- 核心修复：处理 ssh.socket 问题 ----------------------
disable_socket_activation() {
    echo "处理 Systemd Socket 激活配置..."
    
    # 停止并禁用 ssh.socket
    sudo systemctl stop ssh.socket 2>/dev/null
    sudo systemctl disable ssh.socket 2>/dev/null
    
    # 确保 ssh.service 启用并启动
    sudo systemctl enable --now ssh.service 2>/dev/null
    
    # 覆盖默认 socket 配置（如果存在）
    if [ -f "$SSH_SOCKET_CONF" ]; then
        sudo sed -i 's/ListenStream=.*/ListenStream=22345/' $SSH_SOCKET_CONF
        sudo systemctl daemon-reload
        sudo systemctl restart ssh.socket 2>/dev/null
    fi
}

# ---------------------- 原有功能增强 ----------------------
# 确保配置目录存在
init_ssh_config() {
    if [ ! -d "$SSH_CONFIG_DIR" ]; then
        sudo mkdir -p $SSH_CONFIG_DIR
        echo "已创建配置目录 $SSH_CONFIG_DIR"
    fi

    # 写入新配置（覆盖模式）
    echo "Port 22345" | sudo tee $NEW_CONFIG_FILE >/dev/null
    echo "PasswordAuthentication no" | sudo tee -a $NEW_CONFIG_FILE >/dev/null
    echo "PubkeyAuthentication yes" | sudo tee -a $NEW_CONFIG_FILE >/dev/null
}

# 下载并设置公钥（修复路径权限问题）
download_and_set_pub_key() {
    local pub_key_path="/tmp/public_key.pub"
    echo "正在从 $PUB_KEY_URL 下载公钥..."
    if curl -sSf -o "$pub_key_path" "$PUB_KEY_URL"; then
        sudo mkdir -p ~/.ssh
        sudo touch ~/.ssh/authorized_keys
        # 避免重复添加
        if ! grep -qFf "$pub_key_path" ~/.ssh/authorized_keys; then
            cat "$pub_key_path" | sudo tee -a ~/.ssh/authorized_keys >/dev/null
        fi
        sudo chmod 700 ~/.ssh
        sudo chmod 600 ~/.ssh/authorized_keys
        echo "公钥已添加到 ~/.ssh/authorized_keys"
    else
        echo "公钥下载失败，请检查 URL 或网络连接"
        exit 1
    fi
}

# ---------------------- 配置验证增强 ----------------------
check_config_effectiveness() {
    echo -e "\n验证配置状态:"
    local expected_port="22345"
    
    # 检查实际监听端口
    local listening_port=$(sudo ss -tlpn | grep sshd | awk '{print $4}' | cut -d':' -f2)
    if [[ "$listening_port" != *"22345"* ]]; then
        echo "紧急错误: SSH 未监听端口 22345，当前端口: $listening_port"
        echo "可能原因: 1) ssh.socket 未禁用 2) 防火墙冲突 3) 其他服务占用端口"
        exit 1
    else
        echo "端口监听验证通过: 22345"
    fi

    # 使用 sshd -T 检查配置
    sshd_output=$(sudo sshd -T -f /etc/ssh/sshd_config)
    echo "$sshd_output" | grep -q "port $expected_port" && echo "端口配置验证通过" || echo "端口配置未生效"
    echo "$sshd_output" | grep -q "passwordauthentication no" && echo "密码认证已禁用" || echo "密码认证未关闭"
    echo "$sshd_output" | grep -q "pubkeyauthentication yes" && echo "公钥认证已启用" || echo "公钥认证未生效"
}

# ---------------------- 主执行流程 ----------------------
main() {
    # 优先处理 socket 配置
    disable_socket_activation
    
    # 初始化 SSH 配置
    init_ssh_config
    
    # 密钥配置
    download_and_set_pub_key
    
    # 重启服务（使用 Ubuntu 的正确服务名）
    echo "重启 SSH 服务..."
    sudo systemctl restart ssh
    
    # 防火墙配置
    echo "配置防火墙..."
    sudo ufw allow 22345/tcp >/dev/null
    sudo ufw reload >/dev/null
    
    # 验证
    check_config_effectiveness
    echo -e "\n所有配置已完成，请使用以下命令测试连接:"
    echo "ssh -p 22345 -i /path/to/private_key $(whoami)@$(hostname -I | awk '{print $1}')"
}

# 执行主函数
main